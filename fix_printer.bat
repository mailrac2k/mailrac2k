@echo off
cls
echo.
echo "Fixing registry value... by khanhnv.papvnIT"
echo.
REG ADD HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Print\ /f /v RpcAuthnLevelPrivacyEnabled /t REG_DWORD /d 0


echo.
echo "Restarting Print Spooler Service"
pause
echo.
net stop spooler
net start spooler
pause
