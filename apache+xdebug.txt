NameVirtualHost *:8585
Listen 8585

<VirtualHost *:80>
    DocumentRoot ""
    ErrorLog "logs/yourdomain.com-error.log"
    CustomLog "logs/yourdomain.com-access.log" common
    <Directory "">
        Options FollowSymLinks
        AllowOverride All
        DirectoryIndex index.php
        Require all granted
    </Directory>
</VirtualHost>

[xdebug]
zend_extension="D:\xampp_8_1\php\ext\php_xdebug.dll"
xdebug.mode=debug
xdebug.client_host=127.0.0.1
xdebug.client_port=9003
xdebug.start_with_request=yes
